///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Bodie Collins <bodie@hawaii.edu>
/// @date  14 Jan 2022 
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])   
   {
  
   //int n = atoi( argv[1] );
   int n=0;
   int sum=0;

   printf("How many years have you been a crazy cat guy?:");
   scanf("%d", &n);
   
   
   
   //sum up collars for each year  
   while(n>0) 
      {
         sum=sum+n;
         --n;
      }
  

  
   //print the sum of collars 
   printf("You have %d collars\n", sum);
  
      return 0;
   }
